import store from '@state/store'

export default [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => lazyLoadView(import('@views/pages/dashboard/dashboard')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/mon-profil',
    name: 'my_profile',
    component: () => lazyLoadView(import('@views/pages/user/my-profile')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/mes-offres',
    name: 'my_job_offers',
    component: () => lazyLoadView(import('@views/pages/job-offer/my-job-offers')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/domaines/:name',
    name: 'domain_show',
    component: () => lazyLoadView(import('@views/pages/job-offer/domain-show')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/candidat/:id/:job_offer_id?',
    name: 'candidate_show',
    component: () => lazyLoadView(import('@views/pages/candidate/candidate-show')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/agenda',
    name: 'agenda',
    component: () => lazyLoadView(import('@views/pages/events/agenda')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/documents',
    name: 'documents',
    component: () => lazyLoadView(import('@views/pages/ged/documents')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/messagerie',
    name: 'chat',
    component: () => lazyLoadView(import('@views/pages/chat/chat')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/recrutement',
    name: 'recruitment',
    component: () => lazyLoadView(import('@views/pages/job-offer/recruitment')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/recrutement/creation',
    name: 'recruitment_creation',
    component: () => lazyLoadView(import('@views/pages/job-offer/job-offer-creation')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/recrutement/:id',
    name: 'recruitment_show',
    component: () => lazyLoadView(import('@views/pages/job-offer/job-offer-show')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/recrutement-detail/:id',
    name: 'recruitment_show_detail',
    component: () => lazyLoadView(import('@views/pages/job-offer/job-offer-show-detail')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/recrutement/edition/:id',
    name: 'recruitment_edit',
    component: () => lazyLoadView(import('@views/pages/job-offer/job-offer-edit')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/help',
    name: 'help',
    component: () => lazyLoadView(import('@views/pages/help/help')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/parametres',
    name: 'settings',
    component: () => lazyLoadView(import('@views/pages/settings/settings')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/mon-abonnement',
    name: 'subscription',
    component: () => lazyLoadView(import('@views/pages/candidate/subscription')),
    meta: {
      authRequired: true,
    },
    props: (route) => ({ user: store.state.auth.currentUser || {} }),
  },
  {
    path: '/login/google/callback',
    name: 'google_callback',
    component: () => lazyLoadView(import('@views/auth/google')),
    meta: {
      authRequired: false,
    },
  },
  {
    path: '/general_conditions',
    name: 'general_conditions',
    component: () => lazyLoadView(import('@views/auth/general_conditions')),
  },
  {
    path: '/',
    name: 'login',
    component: () => lazyLoadView(import('@views/auth/login')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the my_profile page instead
          next({ name: 'my_profile' })
        } else {
          // Continue to the login page
          next()
        }
      },
    },
  },
  {
    path: '/inscription',
    name: 'register',
    component: () => lazyLoadView(import('@views/auth/register')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the my_profile page instead
          next({ name: 'my_profile' })
        } else {
          // Continue to the login page
          next()
        }
      },
    },
  },
  {
    path: '/confirm-account',
    name: 'confirm-account',
    component: () => lazyLoadView(import('@views/auth/confirm')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the my_profile page instead
          next({ name: 'my_profile' })
        } else {
          // Continue to the login page
          next()
        }
      },
    },
  },
  {
    path: '/forget-password',
    name: 'forget-password',
    component: () => lazyLoadView(import('@views/auth/forget-password')),
    meta: {
      beforeResolve(routeTo, routeFrom, next) {
        // If the user is already logged in
        if (store.getters['auth/loggedIn']) {
          // Redirect to the my_profile page instead
          next({ name: 'my_profile' })
        } else {
          // Continue to the login page
          next()
        }
      },
    },
  },
  {
    path: '/logout',
    name: 'logout',
    meta: {
      authRequired: true,
      beforeResolve(routeTo, routeFrom, next) {
        store.dispatch('auth/logOut')
        const authRequiredOnPreviousRoute = routeFrom.matched.some(
          (route) => route.meta.authRequired
        )
        // Navigate back to previous page, or my_profile as a fallback
        next(authRequiredOnPreviousRoute ? { name: 'login' } : { ...routeFrom })
      },
    },
  },
  {
    path: '/404',
    name: '404',
    component: require('@views/classics/_404').default,
    // Allows props to be passed to the 404 page through route
    // params, such as `resource` to define what wasn't found.
    props: true,
  },
  // Redirect any unmatched routes to the 404 page. This may
  // require some server configuration to work in production:
  // https://router.vuejs.org/en/essentials/history-mode.html#example-server-configurations
  {
    path: '*',
    redirect: '404',
  },
]

// Lazy-loads view components, but with better UX. A loading view
// will be used if the component takes a while to load, falling
// back to a timeout view in case the page fails to load. You can
// use this component to lazy-load a route with:
//
// component: () => lazyLoadView(import('@views/my-view'))
//
// NOTE: Components loaded with this strategy DO NOT have access
// to in-component guards, such as beforeRouteEnter,
// beforeRouteUpdate, and beforeRouteLeave. You must either use
// route-level guards instead or lazy-load the component directly:
//
// component: () => import('@views/my-view')
//
function lazyLoadView(AsyncView) {
  const AsyncHandler = () => ({
    component: AsyncView,
    // A component to use while the component is loading.
    loading: require('@views/classics/_loading').default,
    // Delay before showing the loading component.
    // Default: 200 (milliseconds).
    delay: 400,
    // A fallback component in case the timeout is exceeded
    // when loading the component.
    error: require('@views/classics/_timeout').default,
    // Time before giving up trying to load the component.
    // Default: Infinity (milliseconds).
    timeout: 10000,
  })

  return Promise.resolve({
    functional: true,
    render(h, { data, children }) {
      // Transparently pass any props or children
      // to the view component.
      return h(AsyncHandler, data, children)
    },
  })
}
