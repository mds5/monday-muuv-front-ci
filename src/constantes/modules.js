export default [
  {
    'text' : 'Dashboard',
    'value' : 10,
    'on_candidate' : true
  },
  {
    'text' : 'Recrutement',
    'value' : 20,
    'on_candidate' : true
  },
  {
    'text' : 'Mes annonces',
    'value' : 30,
    'on_candidate' : false
  },
  {
    'text' : 'Messagerie',
    'value' : 40,
    'on_candidate' : false
  },
  {
    'text' : 'Documents',
    'value' : 50,
    'on_candidate' : true
  },
  {
    'text' : 'Agenda',
    'value' : 60,
    'on_candidate' : true
  },
  {
    'text' : 'Besoin d\'aide',
    'value' : 70,
    'on_candidate' : true
  },
  {
    'text' : 'Mon profil',
    'value' : 80,
    'on_candidate' : true
  },
  {
    'text' : 'Facturation',
    'value' : 90,
    'on_candidate' : false
  }
]
