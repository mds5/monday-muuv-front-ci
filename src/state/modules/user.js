import axios from 'axios'
import User from "@src/models/user/User";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  getUser : async (context, data) => {
    const response = await axios.get('api/users/infos/' + data.user_id);
    return new User(response.data.user)
  },
  update : async (context, data) => {
    const response = await axios.put('api/users/' + data.id, data.user)
    return response.data.message;
  },
  delete : async () => {
    const response = await axios.post('/api/users/deleted')
    return response.data.message;
  },
  subscribe : async (context, data) => {
    const response = await axios.post('/api/subscribe', {type : data.type})
    return {
      message : response.data.message,
      user : new User(response.data.user)
    };
  },
  resilliation : async () => {
    const response = await axios.post('/api/resilliation')
    return {
      message : response.data.message,
      user : new User(response.data.user)
    };
  }
}
