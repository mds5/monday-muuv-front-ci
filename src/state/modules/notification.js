import axios from "axios";
import Notification from "@src/models/notification/Notification";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  async all() {
    const response = await axios.get('api/notifications')
    return response.data.notifications.map(notification => {
      return new Notification(notification);
    });
  },
  async allMarkedAsRead() {
    await axios.post('api/notifications/all-marked-as-read')
  }
}
