import axios from "axios";
import Meeting from "@src/models/event/Meeting";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  all() {
    return axios.get('api/events' ).then( response => {
      return response.data.events.map( meeting => {
        const m = new Meeting(meeting);
        console.log( m );
        return m
      });
    });
  },
  find(context, data) {
    return axios.get('api/events/' + data.id ).then( response => {
      return new Meeting(response.data.event);
    });
  },
  create(context, data) {
    return axios.post('api/events', data.meeting ).then( response => {
      return response.data.message;
    });
  },
  update(context, data) {
    return axios.put('api/events/' + data.id , data.meeting ).then( response => {
      return response.data.message;
    });
  },
  delete(context, data) {
    return axios.delete('api/events/' + data.id ).then( response => {
      return response.data.message;
    });
  }
}
