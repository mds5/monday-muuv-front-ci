import axios from "axios";
import Expectation from "@src/models/user/candidate/Expectation";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  all() {
    return axios.get('api/expectations' ).then( response => {
      return response.data.expectations.map( expectation => {
        return new Expectation(expectation);
      });
    });
  },
  link_user(context, data) {
    console.log( data, data.expectations.map(expectation => expectation.id) );
    return axios.post('api/expectations/users',{ expectations : data.expectations.map(expectation => expectation.id) }).then( response => {
      return response.data.message;
    });
  }
}
