import axios from "axios";
import User from "@src/models/user/User";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  getProfiles(context, data){
    return axios.post('api/job_offers/get_profiles/' + data.jobOffer.id ).then( response => {
      const correctProfiles = response.data.correctProfiles.map( jobOffer => {
        return new User( jobOffer )
      });
      const perfectProfiles = response.data.perfectProfiles.map( jobOffer => {
        return new User( jobOffer )
      });
      return {correctProfiles, perfectProfiles}
    });
  }
}
