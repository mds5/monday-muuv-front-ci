import axios from 'axios'
import User from '@src/models/user/User'

export const state = {
  currentUser: getSavedState('auth.currentUser'),
}

export const mutations = {
  SET_CURRENT_USER(state, newValue) {
    state.currentUser = newValue
    saveState('auth.currentUser', newValue)
    setDefaultAuthHeaders(state)
  },
}

export const getters = {
  // Whether the user is currently logged in.
  loggedIn(state) {
    return !!state.currentUser
  },
}

export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {
    setDefaultAuthHeaders(state)
    dispatch('validate')
  },

  // Logs in the current user.
  logIn({ commit, dispatch, getters }, { email, password } = {}) {

    if (getters.loggedIn) return dispatch('validate')

    return axios
      .post('/api/login', { email, password })
      .then((response) => {
        return loginUser(response.data.user, commit)
      })
  },

  // Logs out the current user.
  async logOut({commit}) {
    commit('SET_CURRENT_USER', null)
  },

  loginUser({ commit }, {user}){
    loginUser(user, commit)
  },

  // register the user
  register({ commit, dispatch, getters }, { type, firstname, lastname, companyName, email, password } = {}) {
    if (getters.loggedIn) return dispatch('validate')

    return axios
      .post('/api/register', { type, firstname, lastname, companyName, email, password })
      .then((response) => {
        return response.data
      })
  },

  // register the user
  resetPassword({ commit, dispatch, getters }, { email } = {}) {
    if (getters.loggedIn) return dispatch('validate')

    return axios
      .post('/api/register', { email})
      .then((response) => {
        return response.data
      })
  },

  // Validates the current user's token and refreshes it
  // with new data from the API.
  validate({ commit, state }) {

    if (!state.currentUser) return Promise.resolve(null)

    return axios
      .get('/api/login')
      .then((response) => {
        const user = new User(response.data.user)
        commit('SET_CURRENT_USER', user)
        return user
      })
      .catch((error) => {
        if (error.response && error.response.status === 401) {
          commit('SET_CURRENT_USER', null)
        }
        return null
      })
  },
}

// ===
// Private helpers
// ===

function getSavedState(key) {
  return JSON.parse(window.localStorage.getItem(key))
}

function saveState(key, state) {
  window.localStorage.setItem(key, JSON.stringify(state))
}

function setDefaultAuthHeaders(state) {
  axios.defaults.headers.common.Authorization = state.currentUser ? 'Bearer ' + state.currentUser.remember_token : ''
}

function loginUser(userData, commit){
  const user = new User(userData)
  commit('SET_CURRENT_USER', user)
  return user
}
