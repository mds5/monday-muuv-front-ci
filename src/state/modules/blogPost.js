import axios from 'axios'
import BlogPost from '@src/models/dashboard/BlogPost'

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  getArticles: async function() {
    const response = await axios.get(
      'https://monday-muuv.fr/wp-json/wp/v2/posts'
    )
    let articles = []
    for (let i = 0; i < response.data.length; i++) {
      const blogPostInfos = response.data[i]
      const image = await axios.get(
        blogPostInfos._links['wp:featuredmedia'][0]['href']
      )
      blogPostInfos.image = image.data.guid.rendered
      articles.push(new BlogPost(blogPostInfos))
    }
    return articles
  },
}
