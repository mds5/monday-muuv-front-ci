import axios from "axios";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  create(context, data) {
    return axios.post('api/degrees', data.degree ).then( response => {
      return response.data.message;
    });
  },
  update(context, data) {
    return axios.put('api/degrees/' + data.degree.id , data.degree ).then( response => {
      return response.data.message;
    });
  },
  delete(context, data) {
    return axios.delete('api/degrees/' + data.degree.id ).then( response => {
      return response.data.message;
    });
  }
}
