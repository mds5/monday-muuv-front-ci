import axios from "axios";
import User from "@src/models/user/User";
import Message from "@src/models/chat/Message";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  spokespersons() {
    return axios.get('api/spokespersons').then( response => {
      return response.data.spokesPersons.map( spokesPerson => {
        return ({
          user : new User( spokesPerson.user ),
          last_message : new Message( spokesPerson.last_message ),
        })
      })
    });
  },
  getConversation(context, data) {
    return axios.get('api/conversation/' + data.user_id).then( response => {
      return response.data.messages.map( message => {
        return new Message( message )
      })
    });
  },
  create(context, data) {
    axios.post('api/messages', data.message)
  }
}
