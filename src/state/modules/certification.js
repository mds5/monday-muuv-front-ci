import axios from "axios";
import Certification from "@src/models/user/candidate/Certification";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  async all() {
    const response = await axios.get('api/certifications')
    return response.data.certifications.map(certification => new Certification(certification))
  },
  async user_certifications(context, data) {
    const response = await axios.get('api/certifications/users/' + data.candidate.id)
    return response.data.certifications.map(certification => new Certification(certification))
  },
  async update_user_certifications(context, data) {
    const response = await axios.post('api/certifications/users', {certifications: data.certifications})
    return response.data.message;
  },
}
