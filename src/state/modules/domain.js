import axios from "axios";
import Domain from "@src/models/job-offer/Domain";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  all() {
    return axios.get('api/domains').then( response => {
      return response.data.domains.map( domain => {
        return new Domain( domain );
      });
    });
  },
  findByName(context, data) {
    return axios.get('api/domains/' + data.name).then( response => {
      return new Domain( response.data.domain );
    });
  },
  link_user(context, data) {
    return axios.post('api/domains/users',{ domains : data.domains.map(domain => domain.id) }).then( response => {
      return response.data.message;
    });
  }
}
