import axios from "axios";

export const state = {}

export const getters = {}

export const mutations = {}

export const actions = {
  create(context, data) {
    return axios.post('api/experiences', data.experience ).then( response => {
      return response.data.message;
    });
  },
  update(context, data) {
    return axios.put('api/experiences/' + data.experience.id , data.experience ).then( response => {
      return response.data.message;
    });
  },
  delete(context, data) {
    return axios.delete('api/experiences/' + data.experience.id ).then( response => {
      return response.data.message;
    });
  }
}
