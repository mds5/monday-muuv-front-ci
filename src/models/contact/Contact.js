export default class Contact {

  user_id
  content
  module
  status

  constructor({user_id, content, module, status}) {
    this.user_id = user_id
    this.content = content
    this.module = module
    this.status = status
  }

  get getClassColor() {
    if( this.status === 10 ){
      return 'badge-danger'
    }else if( this.status === 20 ){
      return 'badge-warning'
    }else {
      return 'badge-succès'
    }
  }

  get getNameStatus() {
    if( this.status === 10 ){
      return 'Nouveau'
    }else if( this.status === 20 ){
      return 'En cours de traitemennt'
    }else {
      return 'Traité'
    }
  }

}
