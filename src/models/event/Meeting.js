import moment from "moment";
import User from "@src/models/user/User";

export default class Meeting {

  id
  title
  description
  creator_id
  start
  start_at
  end
  end_at
  participants

  constructor({id, title, description, creator, creator_id, start_at, end_at,  participants = []}) {
    this.id = id
    this.title = title
    this.description = description
    this.creator = creator
    this.creator_id = creator_id
    this.start_at = start_at;
    this.start = this.start_at;
    this.end_at = end_at ? end_at : this.start;
    this.end = this.end_at ;
    this.participants = [];
    participants.forEach( participant => this.participants.push( new User( participant ) ));
  }

  formatStartAt() {
    return moment( this.start ).fromNow();
  }

  formatEndAt() {
    return moment( this.start ).fromNow();
  }

}
