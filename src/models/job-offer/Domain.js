import JobOffer from "@src/models/job-offer/JobOffer";
import User from "@src/models/user/User";

export default class Domain {

  id
  name
  image_card
  image_show
  job_offers
  candidates
  companies

  constructor({id, name, image_card, image_show,  job_offers = [], candidates = [],  companies = []}) {
    this.id = id
    this.name = name
    this.image_card = image_card
    this.image_show = image_show
    this.job_offers = [];
    job_offers.forEach( job_offer => this.job_offers.push( new JobOffer( job_offer ) ));
    this.candidates = [];
    candidates.forEach( candidate => this.candidates.push( new User( candidate ) ));
    this.companies = [];
    companies.forEach( company => this.companies.push( new User( company ) ));
  }

  getImageCard(){
    if( this.image_card ){
      return "http://localhost:8081/storage/" + this.image_card;
    }
    return "http://localhost:8080/images/domain_card_default.jpg"
  }

  getImageShow() {
    if( this.image_card ){
      return "http://localhost:8081/storage/" + this.image_show;
    }
    return "http://localhost:8080/images/domain_show_default.jpg"
  }

}
