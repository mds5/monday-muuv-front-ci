import moment from "moment";
moment.locale('fr');

export default class Experience {

    id
    candidate
    candidate_id
    name
    start_at
    end_at
    description
    company
    city

    constructor({id, candidate, candidate_id, name, start_at, end_at, description, company, city}) {
        this.id = id
        this.candidate = candidate
        this.candidate_id = candidate_id
        this.name = name
        this.start_at = start_at
        this.end_at = end_at
        this.description = description
        this.company = company
        this.city = city
    }

    formatStartAt() {
        return moment( this.start_at ).format('YYYY');
    }

    formatEndAt() {
        return moment( this.end_at ).format('YYYY');
    }
}
