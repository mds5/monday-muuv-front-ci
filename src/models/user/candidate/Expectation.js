export default class Expectation {

    id
    name
    category_id
    category

    constructor({id, name, category_id, category}) {
        this.id = id
        this.name = name
        this.category_id = category_id
        this.category = category
    }

}
