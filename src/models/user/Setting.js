export default class Setting {

    id
    key

    constructor({id, key}) {
        this.id = id
        this.key = key
    }

}
