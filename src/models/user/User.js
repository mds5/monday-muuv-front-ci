import Domain from "@src/models/job-offer/Domain";
import Certification from "@src/models/user/candidate/Certification";
import Expectation from "@src/models/user/candidate/Expectation";
import Experience from "@src/models/user/candidate/Experience";
import Degree from "@src/models/user/candidate/Degree";

export default class User {

    id
    email
    subscription_type
    avatar
    user
    userable_type
    about
    expectations_collaborators
    good_to_know
    tools
    clients
    sector
    firstname
    lastname
    name
    remember_token
    job_name
    number_missions
    years_experiences
    expected_salary
    domains
    experiences
    certifications
    expectations
    degrees

    constructor({
        id,
        name,
        email,
        subscription_type,
        firstname,
        lastname,
        user,
        remember_token,
        job_name,
        number_missions,
        years_experiences,
        expected_salary,
        userable,
        userable_type,
        about,
        expectations_collaborators,
        good_to_know,
        tools,
        clients,
        medium_age,
        nb_ca,
        nb_collab,
        address,
        sector,
        domains = [],
        degrees = [],
        experiences = [],
        certifications = [],
        expectations = []}
    ) {
        if(user){
            this.id = id
            this.email = user.email
            this.subscription_type = user.subscription_type
            this.name = name
            this.avatar = user.avatar
            this.user = user
            this.userable_type = user.userable_type
            this.firstname = firstname
            this.lastname = lastname
            this.fullname = this.isCandidate() ? firstname + ' ' + lastname : name
            this.remember_token = user.remember_token
            this.certifications = []
            this.domains = []
            this.about = user.about
            this.job_name = job_name
            this.number_missions = number_missions
            this.years_experiences = years_experiences
            this.expected_salary = expected_salary
            this.expectations_collaborators = expectations_collaborators
            this.good_to_know = good_to_know
            this.tools = tools
            this.clients = clients
            this.medium_age = medium_age
            this.nb_ca = nb_ca
            this.nb_collab = nb_collab
            this.address = address
            this.sector = sector
            domains.forEach( domain => this.domains.push( new Domain(domain) ));
            certifications.forEach( certification => this.certifications.push( new Certification( certification )));
            this.experiences = []
            experiences.forEach( experience => this.experiences.push( new Experience( experience )));
            this.degrees = []
            degrees.forEach( degree => this.degrees.push( new Degree( degree )));
            this.expectations = []
            expectations.forEach( expectation => this.expectations.push( new Expectation( expectation )));
        }else{
            this.id = userable.id
            this.email = email
            this.subscription_type = subscription_type
            this.name = userable.name
            this.avatar = userable.avatar
            this.userable = userable
            this.userable_type = userable_type
            this.firstname = userable.firstname
            this.lastname = userable.lastname
            this.about = about
            this.job_name = userable.job_name
            this.number_missions = userable.number_missions
            this.years_experiences = userable.years_experiences
            this.expected_salary = userable.expected_salary
            this.expectations_collaborators = userable.expectations_collaborators
            this.sector = userable.sector
            this.good_to_know = userable.good_to_know
            this.tools = userable.tools
            this.clients = userable.clients
            this.medium_age = userable.medium_age
            this.nb_ca = userable.nb_ca
            this.nb_collab = userable.nb_collab
            this.address = userable.address
            this.fullname = this.isCandidate() ? this.firstname + ' ' + this.lastname : userable.name
            this.remember_token = remember_token
            this.domains = []
            domains.forEach( domain => this.domains.push( new Domain(domain) ));
        }
    }

    media(){
        if(this.avatar){
            return this.avatar
        }
        return '/images/users/default-user.jpg'
    }

    isCandidate(){
        return this.userable_type === "App\\Models\\User\\Candidate";
    }

    isCompany(){
        return this.userable_type === "App\\Models\\User\\Company";
    }

    getSubscription() {
        if(this.subscription_type === 1){
            return 'DÉCOUVERTE'
        }else if(this.subscription_type === 2){
            return 'PROFESSIONEL'
        }else {
            return 'ENTERPRISE'
        }
    }

}
