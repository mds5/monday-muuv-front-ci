import moment from "moment";

export default  class Notification {

  id
  name
  content
  icon
  color
  link
  is_read
  created_at

  constructor({id, data, is_read, created_at}) {
    this.id = id
    this.name = data.name
    this.content = data.content
    this.icon = data.icon
    this.color = data.color
    this.link = data.link
    this.is_read = is_read
    this.created_at = created_at
  }

  formatCreatedAt() {
    return moment( this.created_at ).fromNow();
  }

}
