export default class BlogPost {
  id
  title
  content
  image

  constructor({ id, title, excerpt, image }) {
    this.id = id
    this.title = title.rendered
    this.content = excerpt.rendered
    this.image = image
  }
}
